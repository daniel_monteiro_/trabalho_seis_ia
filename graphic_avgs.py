# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import datetime


def generate_graphic_avg(list_rounds):
    list_plot = []
    for a in list_rounds:
        list_plot.append((a.media_pontos))

    plt.plot(list_plot)
    plt.ylabel("Desempenho cluster")
    plt.xlabel("Rodadas de execução")

    plt.savefig("graphics/avg_" + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')