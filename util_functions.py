import math


def calc_distance_two_points(listone_points, listtwo_points): # Passo 2: Calcular a distância entre dois ponto;
    a = (float(listtwo_points[0]) - float(listone_points[0])) * (float(listtwo_points[0]) - float(listone_points[0]))
    b = (float(listtwo_points[1]) - float(listone_points[1])) * (float(listtwo_points[1]) - float(listone_points[1]))
    c = (float(listtwo_points[2]) - float(listone_points[2])) * (float(listtwo_points[2]) - float(listone_points[2]))
    d = (float(listtwo_points[3]) - float(listone_points[3])) * (float(listtwo_points[3]) - float(listone_points[3]))

    distance = a + b + c + d

    return float(math.sqrt(distance))


def sum_items(list_items):
    sum_i = 0.0
    for i in list_items:
        sum_i += i

    return sum_i


def calc_distance_n_points(list_of_points):
    sum_distances = 0.0
    for points in list_of_points:

        for p in points:
            sum_distances += p.distance_from_centre

    return float(sum_distances) / 1000.0


def get_avg_point(list_of_points_list):

    total = len(list_of_points_list)

    x1 = 0.0
    x2 = 0.0
    x3 = 0.0
    x4 = 0.0
    for x in list_of_points_list:
        x1 += float(x[0])
        x2 += float(x[1])
        x3 += float(x[2])
        x4 += float(x[3])

    m_x1 = x1 / total
    m_x2 = x2 / total
    m_x3 = x3 / total
    m_x4 = x4 / total

    return [str(m_x1), str(m_x2), str(m_x3), str(m_x4)]
    # return [(m_x1), (m_x2), (m_x3), (m_x4)]


def get_avg_point_class(list_of_points_list):

    total = len(list_of_points_list)

    x1 = 0.0
    x2 = 0.0
    x3 = 0.0
    x4 = 0.0
    for x in list_of_points_list:
        x1 += float(x.list_point[0])
        x2 += float(x.list_point[1])
        x3 += float(x.list_point[2])
        x4 += float(x.list_point[3])

    m_x1 = x1 / total
    m_x2 = x2 / total
    m_x3 = x3 / total
    m_x4 = x4 / total

    return [str(m_x1), str(m_x2), str(m_x3), str(m_x4)]


def points_are_equals(point_list_one, point_list_two):

    return point_list_one == point_list_two


def all_groups_are_equals(dict_points_one, dict_points_two):
    r = False

    for (k, v), (l, m) in zip(dict_points_one.items(), dict_points_two.items()):
        if points_are_equals(v, m):
            r = True
        else:
            r = False
            break

    return r

#
# listone = [
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311']
# ]
#
# listtwo = [
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#     ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311']
# ]


# print(all_groups_are_equals(listone, listtwo))


# print(points_are_equals(['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#                         ['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311']))


# print(get_avg_point([['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#       ['1.04190381795021', '-0.245915338706126', '0.018402094200778', '-0.457808536273377']]))


# print(calc_distance_two_points(['1.38339540794225', '-0.571542813997174', '0.262984143500645', '-0.0646178341855311'],
#       ['1.04190381795021', '-0.245915338706126', '0.018402094200778', '-0.457808536273377']))
