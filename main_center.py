from util_functions import *

from graphic_avgs import *


class PointDistanceCentroid():
    list_point = []
    distance_from_centre = 0.0
    id_centroid = 0


class GroupByCentroid():
    id_centroid = 0
    list_point_centroid = []
    list_new_point_centroid = []
    lists_points_his = []


class AvgCentroidRound():
    round = 0
    media_pontos = 0.0


def get_avg_points_round(list_groups_finished):
    list_of_points = []

    for g in list_groups_finished:
        list_of_points.append(g.lists_points_his)

    return calc_distance_n_points(list_of_points)


def print_dicionario_item_lista(dicionario):

    for k, v in dicionario.items():
        print(str(k) + ":")
        print(v)
        print("\n")


def print_data_groups(list_groups):

    for v in list_groups:
        print("CENTROID - ID " + str(v.id_centroid) + "\n\n")
        print("Ponto selecionado:")
        print(v.list_point_centroid)
        print("Novo ponto médio:")
        print(v.list_new_point_centroid)

        # print("Seus pontos mais próximos")
        # for p in v.lists_points_his:
        #     print("Ponto: ")
        #     print(p.list_point)
        #     print("Distância: " + str(p.distance_from_centre) + "\n")

        print("Quantidade total de pontos:" + str(len(v.lists_points_his)))

        print("\n\n\n\n\n")


def print_data_avg_rounds(list_avg_rounds):

    for round in list_avg_rounds:
        print("RODADA " + str(round.round) + "\n")
        print(round.media_pontos)
        print("\n\n\n")


def get_centroids(until_qtd):
    dicionario_centroids = {}

    file_centroids = open("data.set/agrup_centroides_Q1.csv", "r")
    file_centroids_content = file_centroids.read()

    list_lines = file_centroids_content.split("\n")

    for i in range(1, until_qtd+1):
        list_linha = list_lines[i].split(",")
        list_linha.pop(0)

        dicionario_centroids.__setitem__(i, list_linha)

    return dicionario_centroids


def get_centroid_num_point_centroid_near(list_point, dict_centroids):

    lesser_point = 0.0
    centroid_index = 0
    for k, v in dict_centroids.items():
        distance = calc_distance_two_points(list_point, v)

        if lesser_point == 0.0:
            if distance > lesser_point:
                lesser_point = distance
                centroid_index = k
        else:
            if distance < lesser_point:
                lesser_point = distance
                centroid_index = k

    return centroid_index, lesser_point


def get_point_from_agrup(i):
    file_points = open("data.set/agrupamento_Q1.csv", "r")
    file_points_content = file_points.read()

    list_return = file_points_content.split("\n")[i].split(",")

    return list_return


def are_in_history(list_hitory, list_new_point):

    for lh in list_hitory:
        if lh == list_new_point:
            return True


def get_list_transformed(list_points):
    lista = []

    for p in list_points:
        lista.append(p.list_point)

    return lista


def get_dict_init(list_groups):
    dict_return = {}

    for g in list_groups:
        dict_return.__setitem__(g.id_centroid, get_list_transformed(g.lists_points_his))

    return dict_return


def get_new_group_with_his_points(g, list_g_select):
    group = GroupByCentroid()

    list_points = []
    for gru in list_g_select:
        if g.id_centroid == gru.id_centroid:
            list_points.append(gru)

    group.id_centroid = g.id_centroid
    group.list_point_centroid = g.list_point_centroid
    group.list_new_point_centroid = get_avg_point_class(list_points)
    group.lists_points_his = list_points

    return group


i = 2

lista_rounds = []

round = 1

while i < 12:

    i += 1

    list_data_round = []

    print(str(i) + " RODADA:\n\n")

    dict_centroids = get_centroids(i)

    list_group = []

    for l, w in dict_centroids.items():
        group = GroupByCentroid()
        group.id_centroid = l
        group.list_point_centroid = w

        list_group.append(group)

    j = 1

    list_selected_group = []
    while j < 1001: # Para separar os pontos dos grupos
        point = get_point_from_agrup(j)

        near_centroid, distance = get_centroid_num_point_centroid_near(point, dict_centroids)

        p_center = PointDistanceCentroid()
        p_center.id_centroid = near_centroid
        p_center.list_point = point
        p_center.distance_from_centre = distance

        list_selected_group.append(p_center)
        j += 1

    # Colocar em uma nova lista os grupos com seus pontos
    list_groups_with_his_points = []

    for g in list_group:
        new_group = get_new_group_with_his_points(g, list_selected_group)
        list_groups_with_his_points.append(new_group)

    # if (i == 3):
    #     print_data_groups(list_groups_with_his_points)

    #Agora é procurar a estabilidade:
    # Depois de gerar uma nova média devo verificar se essa média é igual a média anterior. Se sim,
    # paro a rodada do centroid(linha selecionada) e vou para outro

    dict_init_points_avg = {}
    dict_variation_avg = {}
    for g in list_groups_with_his_points:
        dict_init_points_avg.__setitem__(g.id_centroid, g.list_new_point_centroid)

    complete_list_group = []
    while not all_groups_are_equals(dict_init_points_avg, dict_variation_avg):

        j = 1

        list_selected_group = []
        while j < 1001:  # Para separar os pontos dos grupos
            point = get_point_from_agrup(j)

            if len(dict_variation_avg) == 0:
                near_centroid, distance = get_centroid_num_point_centroid_near(point, dict_init_points_avg)
            else:
                near_centroid, distance = get_centroid_num_point_centroid_near(point, dict_variation_avg)

            p_center = PointDistanceCentroid()
            p_center.id_centroid = near_centroid
            p_center.list_point = point
            p_center.distance_from_centre = distance

            list_selected_group.append(p_center)
            j += 1

        # Colocar em uma nova lista os grupos com seus pontos
        list_groups_with_his_points = []

        for g in list_group:
            new_group = get_new_group_with_his_points(g, list_selected_group)
            list_groups_with_his_points.append(new_group)

        complete_list_group = list_groups_with_his_points

        # if (i == 3):
        #     print_data_groups(complete_list_group)

        if len(dict_variation_avg) > 0:
            dict_init_points_avg = dict_variation_avg

        dict_variation_avg = {}
        for g in list_groups_with_his_points:
            dict_variation_avg.__setitem__(g.id_centroid, g.list_new_point_centroid)

    # if (i == 3):
    print_data_groups(complete_list_group)

    avg = AvgCentroidRound()
    avg.round = round
    avg.media_pontos = get_avg_points_round(complete_list_group)

    lista_rounds.append(avg)

    round += 1


print_data_avg_rounds(lista_rounds)
generate_graphic_avg(lista_rounds)
